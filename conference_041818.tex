\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\graphicspath{{images/}}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
    
\begin{document}

\title{Emerging Wireless Technologies: \\Terahertz communication
}

\author{%
	\IEEEauthorblockN{Christophe Verdonck}
	\IEEEauthorblockA{%
		\textit{Dept. of Mathematics and Computer Science} \\
		\textit{University of Antwerp, Belgium}\\
		\textit{christophe.verdonck@student.uantwerpen.be}
	}
	\and
	\IEEEauthorblockN{Lander Geeraert}
	\IEEEauthorblockA{%
		\textit{Dept. of Mathematics and Computer Science} \\
		\textit{University of Antwerp, Belgium}\\
		\textit{lander.geeraert@student.uantwerpen.be}
	}
}


\maketitle

\begin{abstract}
The Terahertz frequency band is envisioned as one of the possible resources to be utilized for wireless communication in future networks. Communication over this band will feature a number of attractive properties, including terabit-per-second link capacities, miniature transceivers and, potentially, high energy efficiency.
However, before commercial solutions can be developed, a number of specific research challenges have to be addressed first. In this paper we provide an overview of the Terahertz band and the specific challenges that arise due to its unique properties. We then discuss how this band opens the door to novel paradigms like the Internet of Nano Things or can be used to improve already existing technologies like 5G. 
\end{abstract}

\begin{IEEEkeywords}
wireless technologies, terahertz communication, the internet of nano things, B5G
\end{IEEEkeywords}

\section{Introduction}
According to Edholm’s law, the demand for point-to-point bandwidth in wireless short-range communications has doubled every 18 months over the last 25 years. Eventually, this will lead to the convergence of the data rates achieved by wired and wireless networks \cite{edholm}. Following this trend, wireless Terabit-per-sconds (Tbps) links are expected to become a reality within the next 5 years. Past improvements on throughput were mainly based on either the advance of modulation techniques or the exploitation of advanced Multiple Input Multiple Output (MIMO) schemes. However, further improvement is limited due to the scarcity of available bandwidth.
In order to achieve these extremely high data rates, new spectral bands need to be explored. In particular, the Terahertz (THz) band is envisioned as the key wireless technology to satisfy these demands. However, some challenges that complicate the use of this band first have to be addressed before commercial applications can be developed. It is the goal of this paper to provide an overview of the THz band and its peculiarities. Furthermore, we explore how this technology is foreseen to enable the Internet of Nano Things (IoNT) paradigm and may lead to mobile networks that go beyond 5G.

The outline of the remainder of this paper is as follows. In Section \ref{sec:Thz}, we introduce the THz band and its unique challenges. The impact THz communication has on specific layers in the protocol stack is discussed in Section \ref{sec:stack}. Section \ref{sec:B5G} examines how the shift to THz communication can lead to beyond fifth generation mobile networks. In section \ref{sec:IoNT}, we discuss the Internet of Nano Things. Finally, Section \ref{sec:conclusion} concludes this paper.
\section{The Terahertz Band}
\label{sec:Thz}
The THz band is defined as the spectral band that spans the frequencies between 0.1 to 10 THz.
THz band is, to some extend, a unique frequency region. While the bands above and below THz band (namely, the microwave/mmWaves and the infrared) have already been extensively explored, THz remains one of the least-studied zones in the electromagnetic (EM) spectrum. Communicating in this frequency band proves difficult as the approaches for generating EM signals used in microwave and infrared technologies are highly inefficient for THz. This lack of a practical technology for generating and detecting radiation is known as the \emph{Terahertz gap}.
Despite this fact, some promising solutions for the generation of THz waves have been proposed, such as Graphene-based plasmonic nanoantennas \cite{novelantenna}. The progress in this field is, however, slow and thus prevents the rapid expansion of THz communications.

In addition to generating THz signals, there are many other challenges in the realization of efficient and practical THz band communication. The channel model for the THz band is significantly different than that of lower frequency bands. THz signals experience a high path loss. One aspect of this path loss is the spreading loss, which accounts for the attenuation due to the expansion of the wave as it propagates through space. This attenuation depends only on the signal frequency and the transmission distance. A higher frequency is hence expected to have a shorter range. However, in addition to this spreading loss, THz signals also suffer from molecular absorption. As molecules have resonant frequencies in the THz spectrum, they absorb the wave which leads to additional attenuation. This phenomenon depends on the concentration and the particular mixture of molecules encountered along the path, as different molecules have different resonant frequencies. As a result, the THz channel is very frequency-selective. This is illustrated in \figurename{ \ref{fig:channel}, where the path loss is shown for line-of-sight (LOS) propagation in the THz band. For distances much below 1 m, molecular absorption is almost negligible. However, for transmission distances over 1 m, many resonances become significant and the frequency windows in which we can transmit without having high absorption losses become narrower. In addition to weakening the transmitted signal, molecular absorption also introduces noise \cite{noise}.
Due to the presence of obstacles, LOS transmissions might not always be possible. This results in additional reflection and/or scattering losses.
With ultra-broad bandwidth, each frequency component in the transmitted signal experiences different attenuation and delay. This frequency dispersion effect, or equivalent distortion in the time domain are not captured by current multi-path channel models and need to be addressed in order to achieve reliable end-to-end communication.
\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{thz_channel.png}
	\caption{Path loss in the THz band for different transmission distances.}
	\label{fig:channel}
\end{figure}
\section{THz Protocol Stack Impact}
\label{sec:stack}
Existing protocol stacks are insufficient for the use of THz communication networks. Many challenges must be addresed at different layers of the protocol stack to accomodate the ultra-broadband communication that comes with THz networks. Below, we give a brief overview of challenges at each layer of the stack in a bottom-up fashion.

\subsection{Physical Layer}
As we saw in the previous section, the ultra-broadband bandwidth associated to each transmission window in the THz band changes drastically with small variations in the distance due to the effect of molecular absorption. This means the amount of bandwidth available changes with regard to the transmission distance. This requires the development of different modulations for different applications, based on the targeted distance. To address the high path loss of the channel and the power limit of a single transceiver, the use of very large antenna arrays has been proposed to form a massive MIMO sytem \cite{UM-MIMO}. This allows for razor-sharp beamforming as well as spatial multiplexing in order to increase capacity. As the huge bandwidth provided by the THz band exeeds the bandwith provided by a single antenna, it is proposed to use different length antennas in the same array, thus supporting multiple transmission windows. This requires the development of algorithms to dynamically combine different antenna arrays and modes of operation in order to accomodate different users with different needs at different distances.
\subsection{Link Layer}
Novel Medium Access Control (MAC) protocols are required for THz band communication networks. As the THz band provides devices with a very large bandwidth, a typical Carrier-Sense Multiple Access (CSMA) approach is not required as there is ample bandwith to send messages. In addition, this very large bandwidth results in very high bit rates and thus very short transmission times, which results in very low collission probabilities.
Another aspect of the link layer is the choice of the optimal packet size. When transmitting at very high bit rates, even very long packets occupy the channel for extremely short periods of time. However, long packets are more likely to suffer from channel errors which requires adequate error detection and correction schemes. In addition, the transmission of long packet may lead to buffering problems at the receiver or link layer congestion problems. An analysis of an optimal packet size for different applications is thus needed.
\subsection{Network Layer}
Routing in the THz band becomes more challenging due to both the limited communication distance as well as the utilization of highly directional antennas. It is impossible for each node to communicate directly with its nearest router. This suggests the use of a multi-hop hierarchical network structure. This is highly beneficial for THz communication as nodes that are close to each other will have access to much wider bands and thus will be able to transmit at high bit rates. Furthermore, this enables nodes to reduce transmission power which results in major energy savings. New routing algorithms must be designed that take into account the physical and link layer of THz band networks. More specifically, these algorithms should explore metrics that capture the molecular composition of the channel such that, for example, routes through lower-humidity areas are prioritized.
\subsection{Transport Layer}
The transmission rates promised by THz networks will result in a dramatic increase in traffic through the network and internet in general. This calls for the revision of well-established transport layer protocols such as TCP as to prevent and control network congestion issues. As most of the traffic over the internet is transported by TCP, it seems reasonable to modify TCP in order to better support this change in data traffic while at the same time keeping backwards compatibility.
\section{Beyond 5G}
\label{sec:B5G}
The advent of 5G will result in mobile networks with high capacity that can handle the never-ending increase in the number of mobile connected devices. With 5G comes a new air interface, which is known as New Radio (NR). The specification of NR is subdivided into two frequency bands, FR1 (below 6 GHz) and FR2 (24 GHz and up). With the latter band comes the ability to support higher data transfer speeds of up to 100 Gbps. However, in order to support future applications like ultra-high-definition video, a further increase in speed is required. It becomes increasingly difficult to support higher data rates in the current frequency band, as the maximum bandwidth defined for FR2 is 400 MHz. Bandwidths in the order of multiple GHz (a few tens of GHz, up to one THz) may be needed for efficient high capacity data delivery. Such wide contiguous blocks of bandwidth are extremely hard to find below 90 GHz but are abundant in higher frequencies above 90 GHz, particularly in the THz frequency band.
Therefore, the use of THz band for next generation small cells is being considered. This would provide ultra-high-speed data communication within coverage areas of up to 10 m. 

Small cells have to operate in indoor as well as outdoor environments, where weather can have a significant effect on signal attenuation due to the molecular absorption.
When signals are transmitted in the THz band, the data rate drops considerably for NLOS links due to reflection and/or scattering losses on rough surfaces \cite{scattering1,scattering2}. As NLOS communication is unrealistic in indoor or urban environments with mobile nodes, these challenges must be addressed in order to use THz for small cell communication. One proposed solution is the use of smart antennas that are designed to provide mirror-assisted coverage \cite{smartantennas}. These smart antennas are accomodated with a number of movable dielectric mirrors that are able to adaptively reflect and direct beams from other antennas, leading to increased signal power and gain in specific locations. Dielectic mirrors can reflect THz signals with a minimal amount of attenuation. This introduces the concept of virtual LOS. It is envisioned that the dielectric mirrors will have a motor attached that allows them to rotate and adaptively change their configuration. This in turn satisfies the vision of Software Defined Networking (SDN), where the mirrors could be managed by the control plane. However, this does require the control plane to know the precise location of the transmitter and receiver so that mirrors can adaptively change their angles in order to provide virtual LOS between them. Although this approach promises improvement in terms of capacity, received power and path loss, the use of smart antennas does seem to imply careful network planning, in order to provide optimal coverage.

The use of THz for small cells limits the communication distance of individual nodes and reduces the coverage of a single small cell. In the presence of mobile nodes, this results in frequent handovers. When transmitting at very high data rates, the speed of the handover process becomes crucial. New intelligent algorithms should therefore be developed that take into account the speed and direction of a node to make the handover process as smooth as possible.

\section{The Internet of Nano Things}
\label{sec:IoNT}
Most of us have already heard about the Internet of Things (IoT); which simply said is trying to connect a whole lot of devices to the internet with the purpose of making them \emph{smarter} in some way, more helpful to their users. In recent years we have all seen some examples of the IoT, some of which are fairly useful (smart security cameras which can be accessed everywhere, controlling the lights in your house over the internet), others seem rather strange (sending tweets from your refrigerator).

Trying to make the internet of things mainstream and bringing it into our daily ives has brought its own set of challenges which have required us to write new ways for these devices to communicate, new protocols (which might be more battery efficient), seek out new security measures, etc.

However, while the IoT might not yet have taken over most people’s daily lives, we are already looking beyond that. Next to the IoT, we also have the Internet of Nano Things (IoNT); which means we want to interconnect a large amount of nanoscale devices to create a network of these nanodevices which can be used for a wide variety of purposes.

One of the most widely used examples of the IoNT is a network of nano devices, biosensors, which are placed throughout a human body, which then can communicate with a nanorouter (which in itself can be accessed through a gateway, such as the phone of the person) to provide some healthcare provider with information about their patient. Such a patient, whom is constantly observed by the nano-nodes, can get constant support and/or observation from his doctors, which might be critical if the patients health situation is unstable.

\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{bionanonetwork.png}
	\caption{Intrabody nanonetworks for healthcare applications.}
	\label{fig:nanol}
\end{figure}

There are some other examples as well, of course, such as an ‘interconnected office’ where all our tools and devices are monitored in some way by these nano devices. Another example might be the placement of nano devices in high density places (e.g., airports and shopping malls) to track the propagation of viral diseases as to better understand how these diseases are spread and how they affect different people. In the same way such nano networks can generally be used for purposes where we need to gather data over large areas to get an overview of how certain data (such as that of air pollution) differs throughout that area instead of having to determine this data on a few, or even just a single point. It is quite clear that these nano devices have a very wide area of application, there are already many ideas, but there are undoubtedly many more to come.

\subsection{Challenges}
As mentioned before, just as there are some challenges still to overcome with IoT, there are a lot more challenges still to overcome with IoNT. Communication challenges (e.g., determining the frequency band of operation, channel modeling, information modulation), protocols for nanonetworks, security measures, system architectures, system management, timekeeping (and more) are all challenges which we still have to overcome in some way. Some of these already have proposed solutions, but most of them are still in the early stages of research and development. We’ll go a bit deeper into some of these challenges and why they are so important.

A first such challenge is security. Hopefully the previous example of the use of IoNT (a human body which is host to a nanonetwork) makes it immediately clear why there is a need for security measures in IoNT. While the example only mentions sensors why measure some element of the body, they could just as well be tiny regulators, which control some aspect of the body (instead of measure it). In both cases we would not want an adversary take control over the information which is being sent by and to these devices.
 
	If an adversary could fake the results which are generated from the sensors, the health care providers could be given wrong information about the medical state of their patient (which in itself can lead to a range of problems). If the devices are regulators, an adversary could try to take control over these devices and cause serious harm to the patient.
	
	Security brings a lot of complexity. While a normal computer (and even smaller devices such as smartphones these days) are capable of performing serious security measures. This is because these devices are becoming larger and more powerful, but when we speak of nano devices, there are some compromises we have to take into account.
	
	Another possible challenge might be the placement of these devices, a problem which is easily overlooked as it is not directly related to the actual functioning of these devices. Nevertheless, if we look at the earlier example (a human body which is a host to a nano network), we have to keep in mind that these devices will need to have to find their way into the body of the person. Since we probably want to gather specific data from our patient, just letting them swallow a pill filled with nano nodes won't do the trick. We also have to take into account the actual location of the devices as this might put other restrictions on the working of the devices (Research has already shown us that THz communication is susceptible to molecular absorption).

\subsection{Compromises}

A first such compromise; depending on their effective size, nano devices most presumably can not have batteries and as such rely on other ways of gathering/harvesting energy (such as generating energy by nanowire vibrations or by using biochemicals in the environment as fuel. We don’t want to have to replace ten, hundreds or even thousands of these devices just because they ran out of energy (even if we wanted to do that, it would not be feasible). Even if such a device were to have a battery, it would still be limited by what it actually can do by the computational power (which is thus another compromise), since not only are these devices smaller in size than a regular processing device, they also have to avoid spending too much energy (because of the aforementioned reason) on the actual processing.

Of course we would not want to put more processing into the nano nodes themselves than is needed, but the nano router (and possibly nano interface) would have to be rather small devices themselves, so the previous problems do still apply. These might be some straightforward problems, but they are nevertheless problems (or rather properties of nano devices and networks) which we have to deal with.


\subsection{Adressing with Nanomachines}

While with the IoT, we can assign a unique ID to each and every device in the network; it's not so simple to assign a unique address to each nano node in the nano network. This is mainly caused by the fact the we would need complex synchronization (and coordination) between the nano elements to achieve that. If we now imagine a nano network with hundreds or even thousands of nano nodes, it's easy to see that we would need very long addresses to achieve the inter networking of these nodes.

However, nano devices connected in such a nano network are not the same as a few computers on some LAN network. It seems highly unlikely that an application would need access to, or needs to communicate with one single nano node (and even if this were the case, this could probably better happen via an interface), but rather to have contact or information from a certain type of nano nodes.

In the same way of thinking, we could also allow for a more relaxed coordination where we say that all nano nodes of the same type should react in the same way. Thus different nodes will react the same (or not) depending on their internal state or type.

\subsection{Nanoscale communication}

There are several proposed ways to achieve nanoscale communication; graphene based nano antennas\cite{iont} (this by means of the THz range) and molecular communication\cite{iont2} are just two of such proposed methods.

Molecular communication could be realized in several different ways. One such way is by encoding/converting information into biomolecules and then transporting these molecules to a recipient nanodevice which in itself decodes the molecule back to the original information. This could be done in several ways such as calcium signaling, bacteria and virus nanonetworks, and by using neurons. Using bacteria and viruses (which can carry generic data) in particular is suited for sensors which are capable of encoding (and decoding) information which is stores inside of DNA.

Of course, if we were to use such an unusual/new method of communication, we would have to adapt or reinvent protocols to be better suited for the nanocommunication properties.

\subsection{Further Research}

Of course, a lot of the challenges and problems which IoNT faces, have already been the subject of research. There has been research in the field of timekeeping, and more specifically; persistent clocks for batteryless sensing devices.\cite{Persistentclocks} This research concerns itself with finding a decent solution for persistent clocks on such small devices. Such devices thus don't have a battery, but rather often work by a capacitors which are charged when the device is capable of harvesting energy and can hold a charge for some time after power to the device itself has been lost. This capacitor serves as a way to prevent data loss up to a certain extent.

However the device has no sense of time while it has no power. Thus the research goes over the use of TARDIS and CusTARD, which are methods to keep time by looking at the degradation rate of SRAM memory or looking at the remaining charge of the capacitor respectively.

Timekeeping is just one of the areas which has been researched, as are many others. Mainly such other research has been focused on allowing communication on such a small scale, with such devices. But allowing to do so requires improvements/changes all the way from the physical layer, up to the networking protocols.

\subsection{Subconclusion}

The IoNT is an idea on which a lot of work has already been done. Just like many innovative ideas in the field of computer science, it potentially offers useful applications ranging from life assistance, general convenience and even entertainment, which eventually may become a reality and aid us in our day to day lives.

To this day, there are still many problems to solve with IoNT, problems which often are also present in the context of IoT. There have been multiple cases of IoT devices failing in some aspect ( which could have been prevented. The question then still remains, if we still have so many things to fix with IoT, are we really already ready for IoNT?

\section{Conclusion}
\label{sec:conclusion}
In this paper, we gave an overview of the THz band and its potential application domains. We saw that although this technology is envisioned to satify the need for Tbps wireless links in the future, a lot of challenges still need to be addressed in order to achieve a robust technology. When more mature, this technology will undoubtedly change the way we communicate, with many additional applications sure to emerge.

\begin{thebibliography}{00}
\bibitem{iont}
I. F. Akyildiz and J. M. Jornet, "The Internet of nano-things," in IEEE Wireless Communications, vol. 17, no. 6, pp. 58-63, December 2010.

\bibitem{terahertz}
I. F. Akyildiz, J. M. Jornet and C. Han, "Terahertz band: Next frontier for wireless communications" in Physical Communication, vol. 12, pp 16-32, September 2014

\bibitem{Persistentclocks}
J. Hester, N. Tobias, A. Rahmati, L. Sitanayah, D. Holcomb, K. Fu, W. P. Burleson and J. Sorber, "Persistent Clocks for Batteryless Sensing Devices", in ACM Transactions on Embedded Computing Systems, vol. 15, no. 4, article 77, August 2016
\bibitem{novelantenna}
J. M. Jornet and I. F. Akyildiz, “Graphene-based plasmonic nanoantenna for terahertz band communication in nanonetworks,” IEEE Journal on Selected Areas in Communications, vol. 31, pp. 685–694, December 2013.

\bibitem{noise}
R.M. Goody, Y.L. Yung, Atmospheric Radiation: Theoretical Basis, second ed., Oxford University Press, 1989.

\bibitem{UM-MIMO}
I. F.Akyildiz and J. Jornet Realizing, "Ultra-Massive MIMO (1024 x 1024) communication in the (0.06–10) Terahertz band" , Nano communication Networks, vol. 8, pp. 46-54, June 2016.

\bibitem{scattering1}
C. Jansen, R. Piesiewicz, D. Mittleman, T. Kurner, and M. Koch,“The impact of reflections from stratified building materials on the wave propagation in future indoor terahertz communication systems,” IEEE Trans. Antennas Propag., vol. 56, no. 5, pp. 1413–1419, May 2008.

\bibitem{scattering2}
C. Jansen et al., “Diffuse scattering from rough surfaces in THz communication channels,” IEEE Trans. THz Sci. Technol., vol. 1, no. 2, pp. 462–472, Nov. 2011.

\bibitem{smartantennas}
M. T. Barros, R. Mullins, and S. Balasubramaniam, "Integrated Terahertz Communication With Reflectors for 5G Small-Cell Networks", IEEE transactions on vehicular technology, vol. 66, no. 7, July 2017.

\bibitem{edholm}
Esmailzadeh, Riaz (2007). Broadband Wireless Communications Business: An Introduction to the Costs and Benefits of New Technologies. West Sussex: John Wiley \& Sons, Ltd. p. 10. 

\bibitem{iont1}
M.H. Miraz, M. Ali, "A Review on Internet of Things (IoT), Internet of Everything (IoE) and Internet of Nano Things (IoNT)", 2015 Internet Technologies and Applications (ITA)

\bibitem{iont2}
S. Balasubramaniam, J. Kangasharju, "Realizing the Internet of Nano Things: Challenges, Solutions, and Applications", Computer, vol. 46, issue 2 , February 2013.


\end{thebibliography}


\end{document}
